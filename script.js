

// Quelques variables de départ
var answerA = null;
var answerA = null;
var answerA = null;
var answerA = null;
var time = 0;
var numero = 0;

// Charge le contenu du html depuis le JSON
function changeHtml(a){
	$("#answerA").html(answers[a]["answerA"]);
	$("#answerB").html(answers[a]["answerB"]);
	$("#answerC").html(answers[a]["answerC"]);
	$("#answerD").html(answers[a]["answerD"]);
	$("#question").html(answers[a]["question"]);
	$("#indice").html(answers[a]["indice"]);

	if (a == 9) {
		$("#suivant").html("Terminer");


		UIkit.util.on('#suivant', 'click', function (e) {
           e.preventDefault();
           e.target.blur();
           UIkit.modal.alert('Felicitation ! Vous avez fini le Quiz. Votre score total est de  '+ totalScore +' points !').then(function () {
               console.log(score);
           });
       });
	}

	if (a == 10) {
		$("#js-progressbar").hide();
	}

	answerA = answers[a]["trueA"];
	answerB = answers[a]["trueB"];
	answerC = answers[a]["trueC"];
	answerD = answers[a]["trueD"];
}
changeHtml(numero);

//
function reponse(a) {
	$("#reponse").html(answers[a]["solution"]);
	$("#reponseTitle").html('Réponse :');
}

// Gestion des réponses
function answerColorA(answer){
	if (answer == true) {
		$("#answerA").css('background-color', '#D8F6CE');
		calculeStepScore();
	} else if (answer == false) {
		$("#answerA").css('background-color', '#F5A9A9');
		mauvaiseReponse();
	}
	stopProgress();
}
function answerColorB(answer){
	if (answer == true) {
		$("#answerB").css('background-color', '#D8F6CE');
		calculeStepScore();
	} else if (answer == false) {
		$("#answerB").css('background-color', '#F5A9A9');
		mauvaiseReponse();
	}
	stopProgress();
}
function answerColorC(answer){
	if (answer == true) {
		$("#answerC").css('background-color', '#D8F6CE');
		calculeStepScore();
	} else if (answer == false) {
		$("#answerC").css('background-color', '#F5A9A9');
		mauvaiseReponse();
	}
	stopProgress();
}
function answerColorD(answer){
	if (answer == true) {
		$("#answerD").css('background-color', '#D8F6CE');
		calculeStepScore();
	} else if (answer == false) {
		$("#answerD").css('background-color', '#F5A9A9');
		mauvaiseReponse();
	}
	stopProgress();
}


// Gestion de la progress Bar
function starProgressBar() {
	UIkit.util.ready(function () {
	    var bar = document.getElementById('js-progressbar');
	    var animate = setInterval(function () {
	    	if (stopProgressIndice == true) {
	    		bar.value += 1;
	        time = bar.value;
	        if (bar.value >= bar.max) {
	            clearInterval(animate);
	        	}
	        }
	    }, 100);
	});
}
starProgressBar();


let score = 0;
//Gestion du score
function calculeStepScore(){
	score = (110 - time);
	console.log(score);
	let phraseAjoutScore = 'Bonne réponse : + ' + score + ' points';
	UIkit.notification({message: phraseAjoutScore, status: 'success'})
}

//Mauvaise réponse
function mauvaiseReponse(){
	UIkit.notification({message: 'Mauvaise réponse : + 0 point', status: 'warning'})
}

//Stop
var stopProgressIndice = true;
function stopProgress(){
	stopProgressIndice = false;
	disableButton('answerA');
	disableButton('answerB');
	disableButton('answerC');
	disableButton('answerD');
	activateButton('suivant');
	reponse(numero);

}
disableButton('suivant');

//Activer et désactiver les boutons
function disableButton(c) {
	document.getElementById(c).disabled = true;
}
function activateButton(d) {
	document.getElementById(d).disabled = false;
}

// Décolorer les boutons
function whiteButton(e) {
	$(e).css('background-color', '');
}


let totalScore = 0;
//Evenement de fin d'étape
function stepEnd(){
	let indiceStep = '#step'+(numero+1);
	totalScore = totalScore + score;
	$("#score").html(totalScore);
	$(indiceStep).addClass("uk-active");
	numero = numero+1;
	reset()
}


// reset progressivement la progress bar
function resetProgressBar() {
	UIkit.util.ready(function () {
	    var bar = document.getElementById('js-progressbar');
	    	if (stopProgressIndice == true) {
		    	bar.value = 0;
		        time = bar.value;
	    }
	});
}


let	resetProgress = true;
// le reset 
function reset(){
	score = 0;
	changeHtml(numero);
	activateButton('answerA');
	activateButton('answerB');
	activateButton('answerC');
	activateButton('answerD');

	whiteButton('#answerA'); 
	whiteButton('#answerB'); 
	whiteButton('#answerC'); 
	whiteButton('#answerD');

	stopProgressIndice = true;
	resetProgress = true;
	resetProgressBar()
	disableButton('suivant');
	$("#reponse").html('');
	$("#reponseTitle").html('');
}


